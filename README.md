Personal Finance monitoring for Open Banking via Truelayer

# TrueLayer

I was disappointed by the roll out of so-called [Open Banking] apis because there
was no provision for direct user access. You *have* to go via a third party
registered with the FCA for a minimum of £50k in liability. The loophole is to
find a provider that themselves offer an API and use them as a proxy.

[Open Banking]: https://openbanking.atlassian.net/wiki/spaces/DZ/overview

You will need to [Sign up] for a free [Data API] account in their Production environment and generate a `client_secret`. TrueLayer was recommended by others on [r/UKPersonalFinance] on multiple occasions, I haven't tried any alternatives.

[Sign up]: https://console.truelayer.com/auth
[Data API]: https://docs.truelayer.com/#end-user-authentication
[r/UKPersonalFinance]: https://www.reddit.com/r/UKPersonalFinance/

# Bash vs Python

Python is the correct choice for the structured data and little pipelining in
this project. However, when I'm developing against an API I like to see the
whole request+response as it happens with full headers and bodies, ideally
pretty printed. That is [annoyingly difficult] in python, so I'll be using bash
until I need a data structure.

[annoyingly difficult]: https://stackoverflow.com/questions/16337511/log-all-requests-from-the-python-requests-module/57325050#57325050

# License

Copyright (C) 2020 Phil Morrell

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

On Debian systems, the full text of the GNU General Public License
version 3 can be found in the file `/usr/share/common-licenses/GPL-3'.
